# Práctica Módulo 07 - Liberando Productos - Segunda Parte

## Elegir el tipo de despliegue a utilizar para cada microservicio

Se ha consultado la siguiente fuente de información complementar la toma de decisiones:

* [https://thenewstack.io/deployment-strategies/](https://thenewstack.io/deployment-strategies/)
* [http://blog.itaysk.com/2017/11/20/deployment-strategies-defined](http://blog.itaysk.com/2017/11/20/deployment-strategies-defined)

### Web Service

Este servicio es:

* Front (web).
* Público (lo utilizan los clientes de la empresa).
* El más importante en lo relativo a la imagen de la compañía hacia el exterior.

Se decide utilizar el tipo de despliegue "**Canary**". Al permitir desviar un porcentaje del tráfico a la nueva versión en los despliegues, se consigue:

* No elevar demasiado el coste de Cloud respecto a otros métodos (los recursos de cada versión escalan automáticamente).
* En el peor de los casos (mal funcionamiento de la nueva versión):
  * El impacto sólo es a un porcentaje de los usuarios.
  * El rollback es fácil y rápido.

Se asume:

* Aumentar el tiempo desde que se decide implantar una nueva versión, hasta que la versión anterior es reemplazada.
* Tener más de una versión de la aplicación al mismo tiempo.
* Invertir tiempo del personal de IT en el control del despliegue durante su preparación y evolución.

### Backoffice Service

El servicio es:

* Front (web).
* Interno (lo utilizan los empleados de la empresa).
* Sin relevancia respecto a la imagen de la compañía hacia el exterior.

Se decide utilizar el tipo de despliegue "**Rolling Deploy**", con lo que se consigue:

* Abaratar costes en recursos Cloud.
* Reducir el tiempo desde que se decide implantar una nueva versión, hasta que la versión anterior es reemplazada.
* En el peor de los casos (mal funcionamiento de la nueva versión), el rollback es fácil y suficientemente rápido.

Se asume:

* Tener más de una versión de la aplicación al mismo tiempo.
* En el peor de los casos (mal funcionamiento de la nueva versión), todos los usuarios se ven afectados.

### Pricing Service

El servicio es:

* Back.
* Global (lo utilizan tanto los clientes como los empleados de la compañía).
* Crítico (utilizado por todo el negocio de la compañía, la información en las respuestas debe ser consistente).

Se decide utilizar el tipo de despliegue "**Blue Green**", con lo que se consigue:

* Tener sólo una versión de la aplicación al mismo tiempo (el servicio siempre debe dar el mismo precio a todas las peticiones).
* Dar siempre la misma calidad del servicio sin caída en el rendimiento ocasionada por falta de recursos.
* En el peor de los casos (mal funcionamiento de la nueva versión), el rollback es fácil y rápido.

Se asume:

* Elevar los costes en recursos Cloud.
* En el peor de los casos (mal funcionamiento de la nueva versión), todos los usuarios se ven afectados.

### Notification Service

El servicio es:

* Back.
* Global (lo utilizan tanto los clientes como los empleados de la compañía).
* No crítico (las notificaciones son asíncronas, una demora en el envío de las notificaciones no tiene gran impacto).
* Sin relevancia respecto a la imagen de la compañía hacia el exterior.

Se decide utilizar el tipo de despliegue "**Rolling Deploy**", con lo que se consigue:

* Abaratar costes en recursos Cloud.
* Reducir el tiempo desde que se decide implantar una nueva versión, hasta que la versión anterior es reemplazada.
* En el peor de los casos (mal funcionamiento de la nueva versión):
  * Ningún usuario se ve afectado.
  * El rollback es fácil y suficientemente rápido.

Se asume:

* Tener más de una versión de la aplicación al mismo tiempo (no ofrece problemas).

## Elegir 3 SLOs para comprobar si el servicio de "pricing" está funcionando correctamente

En los objetivos se ha decidido perseguir más la fiabilidad, que la reducción de los tiempos de respuesta:

* Respuestas con código 5xx en menos del 2% de las peticiones.
* Respuesta en menos de 300ms en el 95% de las peticiones.
* Respuesta en menos de 100ms en el 90% de las peticiones.

## Poner ejemplo de traza de una petición de precio para un envío

![Trazas 01](./01-practica/img/trazas_01.png)

En el ejemplo:

* Desde que se realiza la petición, hasta que se procesa por el Web Service, transcurre 1 ms.
  * Desde que el Web Service recibe la petición, hasta que llama al Pricing Service, transcurren 3 ms.
    * Desde que el Pricing Service recibe la petición, hasta que llama a la Cache, transcurren 3 ms.
      * La Cache procesa la solicitud con fallo (no encuentra el dato solicitado), empleando 10 ms.
    * Desde que la Cache retorna el resultado (fallo), hasta que el Pricing Service solicita información la Database, transcurre 1 ms.
      * Desde que la Database recibe las peticiones (3 en paralelo), hasta que se responde la última petición, transcurren 39 ms.
    * Desde que la Database retorna el resultado, hasta que se solicita almacenar el precio en la Cache, transcurren 2 ms.
      * La Cache procesa la solicitud de almacenar el precio, empleando 10 ms.
    * Desde que la Cache termina el almacenamiento del precio, hasta que se retorna al Web Service, transcurre 1 ms.
  * Desde que el Web Service recibe el precio, hasta que lo retorna a la petición recibida, transcurre 1 ms.

## Elegir método de resiliencia en el "web service" para reducir las consecuencias en los picos de tráfico

Si hay congestión (muchas peticiones) en el "web service", se utilizará el método de resiliencia "Adaptive LiFo & CoDel":

* Ignorar peticiones antiguas y atender las nuevas (las antiguas deberían hacer reintentos).
* Reducir la duración de los "timeouts" para las peticiones encoladas durante la congestión.

## Describir proceso para gestionar una caída del servicio "message queue"

* Se produce un fallo de disponibilidad en el servicio "message queue".
* Al cabo de cierto tiempo sin disponibilidad en el servicio, se establece una alerta "firing".
* La alerta se procesa realizando una notificación automática al equipo responsable del servicio por los siguientes medios:
  * Correo electrónico.
  * Slack.
* En la notificación de la alerta hay un enlace a la página web con la última versión del "runbook" relativo al fallo producido.
* El personal de guardia recibe la notificación de la alerta.
* El personal de guardia realiza los pasos especificados en el "runbook" recibido en la alerta para el restablecimiento del servicio.
  * Si no se consigue el restablecimiento del servicio tras pasar el período de tiempo especificado en el "runbook", el personal de guardia deberá comunicar la incidencia a su superior.
* El servicio "message queue" es restablecido.
* El equipo responsable debe completar el informe "Incident Report", con toda la información relativa al fallo y a la recuperación del servicio, con:
  * Fecha.
  * Autores.
  * Resumen.
  * Impacto.
  * Causas principales.
  * Cronograma.
  * Resolución.
* Todos los equipos implicados en el fallo deben completar, en una reunión, el informe "Postmortem", con:
  * Fecha.
  * Autores.
  * Enlace al "Incident Report".
  * Lecciones aprendidas.
  * Acciones futuras.
* El equipo responsable debe revisar si el "runbook" utilizado necesita actualización.
