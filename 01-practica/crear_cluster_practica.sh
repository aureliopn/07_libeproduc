#!/bin/bash

# Párámetros por defecto
NOMBRE_CLUSTER=${1:-practica-libeproduc}
ZONA_CLUSTER=${2:-europe-west1-b}
MAQUINAS_CLUSTER=${3:-n1-standard-2}
MAX_NODES_CLUSTER=${4:-5}
NAMESPACE=${5:-practica}
PASSWORD=${6:-PasswordMuyMuyDificilDeAdivinar0=0}
VERSION_HELM=${7:-v2.14.3}

# Valida si una IP es válida, retorna:
# 0: válida.
# 1: NO válida.
function valid_ip()
{
  local  ip=$1
  local  stat=1

  if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
    OIFS=$IFS
    IFS='.'
    ip=($ip)
    IFS=$OIFS
    [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
    stat=$?
  fi
  echo $stat
}

# Crear clúster
gcloud container clusters create $NOMBRE_CLUSTER \
  --zone $ZONA_CLUSTER \
  --enable-stackdriver-kubernetes \
  --enable-autoscaling \
  --machine-type=$MAQUINAS_CLUSTER \
  --max-nodes=$MAX_NODES_CLUSTER

# Utilizar clúster creado
gcloud container clusters get-credentials $NOMBRE_CLUSTER --zone $ZONA_CLUSTER

# Instalar Helm
cd ./helm
chmod 700 *.sh
./helm_install.sh $VERSION_HELM
cd ..

# Instalar nginx-ingress
kubectl create clusterrolebinding cluster-admin-binding --clusterrole cluster-admin --user $(gcloud config get-value account)
kubectl apply -f ./nginx-ingress/mandatory.yaml
kubectl apply -f ./nginx-ingress/cloud-generic.yaml
LOAD_BALANCER_IP=$(kubectl get services --all-namespaces|grep LoadBalancer|awk '{print $5};')
IP_LOAD_BALANCER_VALIDA=$(valid_ip $LOAD_BALANCER_IP)
until [[ $IP_LOAD_BALANCER_VALIDA -eq 0 ]]; do
  echo "Esperando 10 segundos para obtener IP del balanceador..."
  sleep 10
  LOAD_BALANCER_IP=$(kubectl get svc --all-namespaces|grep LoadBalancer|awk '{print $5};')
  IP_LOAD_BALANCER_VALIDA=$(valid_ip $LOAD_BALANCER_IP)
done
echo "Obtenida IP del balanceador: $LOAD_BALANCER_IP"

# Instalar Prometheus Operator
cd ./monitoring
sudo chmod 700 *.sh
sed -i "s/0.0.0.0/$LOAD_BALANCER_IP/g" monitoring-ingress.yaml
./instalar_prometheus_operator.sh
cd ..
NUMERO_CRDs_PROMETHEUS=$(kubectl get crds | grep 'monitoring' | wc -l)
until [[ $NUMERO_CRDs_PROMETHEUS -eq 5 ]]; do
  echo "Esperando 10 segundos mientras se crean los CRDs de Prometheus..."
  sleep 10
  NUMERO_CRDs_PROMETHEUS=$(kubectl get crds | grep 'monitoring' | wc -l)
done

# Instalar la aplicación
cd ./nc
sudo chmod 700 *.sh
kubectl create namespace $NAMESPACE
./start-kubernetes.sh $PASSWORD $NAMESPACE
cd ..

# Exponer la aplicación
sed -i "s/0.0.0.0/$LOAD_BALANCER_IP/g" ./nginx-ingress/netcore-counter-ingress-redis.yaml
kubectl apply -f ./nginx-ingress/netcore-counter-ingress-redis.yaml --namespace=$NAMESPACE

# Añadir el Service Monitor para la aplicación
kubectl apply -f ./monitoring/counter-service-monitor-redis.yaml --namespace=$NAMESPACE
