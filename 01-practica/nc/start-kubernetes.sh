#!/bin/bash
PASSWORD=${1:-PasswordMuyMuyDificilDeAdivinar0=0}
NAMESPACE=${2:-practica}

kubectl create secret generic redis --from-literal=password=$PASSWORD --namespace=$NAMESPACE
kubectl apply -f 02_redis-pvc.yaml --namespace=$NAMESPACE
kubectl apply -f 03_redis-deployment.yaml --namespace=$NAMESPACE
kubectl apply -f 04_redis-service.yaml --namespace=$NAMESPACE
kubectl apply -f 05_counter-deployment-redis.yaml --namespace=$NAMESPACE
kubectl apply -f 05_counter-deployment-01-06.yaml --namespace=$NAMESPACE
kubectl apply -f 05_counter-deployment-03.yaml --namespace=$NAMESPACE
kubectl apply -f 05_counter-deployment-04.yaml --namespace=$NAMESPACE
kubectl apply -f 05_counter-deployment-05.yaml --namespace=$NAMESPACE
kubectl apply -f 06_counter-service-redis.yaml --namespace=$NAMESPACE
kubectl apply -f 06_counter-service-07.yaml --namespace=$NAMESPACE
