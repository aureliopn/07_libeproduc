# Práctica Módulo 07 - Liberando Productos - Primera Parte

## kube-state-metrics con Prometheus

El script instala todo lo necesario en el clúster, con la configuración definida en el archivo [values.yaml](./01-practica/monitoring/values.yaml):

```bash
helm install --name prometheus stable/prometheus-operator --namespace=monitoring --version v8.3.0 -f ./values.yaml
kubectl apply -f monitoring-ingress.yaml --namespace=monitoring
```

Se puede acceder a las webs de:

* Prometheus: [http://monitoring-prometheus.35.205.124.111.nip.io/](http://monitoring-prometheus.35.205.124.111.nip.io/)
* Alert Manager: [http://monitoring-alertmanager.35.205.124.111.nip.io/](http://monitoring-alertmanager.35.205.124.111.nip.io/)

## Alarmas en Prometheus

Excepto para la alarma "02", se han desplegado "Deployments" que hacen saltar, al menos, una de la alarmas requeridas.

### 01 Pods en estado diferente a "running" durante más de 15 minutos

```yaml
- alert: 01_pod_no_running_15_min
  expr: kube_pod_status_phase{namespace="practica",phase!~"Running"} == 1
  for: 15m
  labels:
    team: "sistemas"
    severity: critical
  annotations:
    summary: "Pods sin ejecutar durante 15 minutos"
    description: "Pod {{ $labels.pod }} en namespace {{ $labels.namespace }} lleva 15 minutos sin estar ejecutándose."
```

![Alerta 01](./01-practica/img/alerta_01.png)

### 02 Uso de CPU de un contenedor mayor al 90% del límite durante más de 15 minutos

```yaml
- alert: 02_contenedor_pasa_90_pct_limite_cpu_15_min
  expr: sum(label_replace(rate(container_cpu_usage_seconds_total{namespace="practica"}[1m]), "container", "$1", "container_name", "(.*)")) by (container, namespace) / sum(kube_pod_container_resource_limits_cpu_cores{namespace="practica"}) by (container, namespace) > 0.9
  for: 15m
  labels:
    team: "sistemas"
    severity: high
  annotations:
    summary: "Contenedor consume más del 90% del límite de CPU durante 15 minutos"
    description: "Contenedor {{ $labels.container_name }} en namespace {{ $labels.namespace }} lleva 15 minutos consumiendo más del 90% del límite de CPU."
```

### 03 Uso de memoria de un contenedor mayor al 80% del límite durante más de 15 minutos

```yaml
- alert: 03_contenedor_pasa_80_pct_limite_mem_15_min
  expr: sum(container_memory_working_set_bytes{namespace="practica"}) by (namespace,container_name) / sum(label_join(kube_pod_container_resource_limits_memory_bytes{namespace="practica"}, "container_name", "", "container")) by (namespace,container_name) > 0.8
  for: 15m
  labels:
    team: "sistemas"
    severity: high
  annotations:
    summary: "Contenedor consume más del 80% del límite de memoria durante 15 minutos"
    description: "Contenedor {{ $labels.container_name }} en namespace {{ $labels.namespace }} lleva 15 minutos consumiendo más del 80% del límite de memoria."
```

![Alerta 03](./01-practica/img/alerta_03.png)

### 04 Uso de CPU de un contenedor por debajo del 50% del solicitado durante más de 15 minutos

```yaml
- alert: 04_contenedor_debajo_50_pct_solicitado_cpu_15_min
  expr: sum(label_replace(rate(container_cpu_usage_seconds_total{namespace="practica"}[1m]), "container", "$1", "container_name", "(.*)")) by (container, namespace) / sum(kube_pod_container_resource_requests_cpu_cores{namespace="practica"}) by (container, namespace) < 0.5
  for: 15m
  labels:
    team: "sistemas"
    severity: high
  annotations:
    summary: "Contenedor consume menos del 50% de su solicitud de CPU durante 15 minutos"
    description: "Contenedor {{ $labels.pod }} en namespace {{ $labels.namespace }} lleva 15 minutos consumiendo menos del 50% de su solicitud de CPU."
```

![Alerta 04](./01-practica/img/alerta_04.png)

### 05 Uso de memoria de un contenedor por debajo del 50% del solicitado durante más de 15 minutos

```yaml
- alert: 05_contenedor_debajo_50_pct_solicitado_mem_15_min
  expr: sum(container_memory_working_set_bytes{namespace="practica"}) by (namespace,container_name) / sum(label_join(kube_pod_container_resource_requests_memory_bytes{namespace="practica"}, "container_name", "", "container")) by (namespace,container_name) < 0.5
  for: 15m
  labels:
    team: "sistemas"
    severity: high
  annotations:
    summary: "Contenedor consume menos del 50% del límite de memoria durante 15 minutos"
    description: "Contenedor {{ $labels.container_name }} en namespace {{ $labels.namespace }} lleva 15 minutos consumiendo menos del 50% del límite de memoria."
```

![Alerta 05](./01-practica/img/alerta_05.png)

### 06 Deployment con número de réplicas disponibles diferente a las esperadas durante más de 30 minutos

```yaml
- alert: 06_replicas_deployment_diferente_esperadas_30_min
  expr: kube_deployment_status_replicas_available{namespace="practica"} != kube_deployment_status_replicas{namespace="practica"}
  for: 30m
  labels:
    team: "sistemas"
    severity: high
  annotations:
    summary: "Deployment no alcanza las replicas esperadas"
    description: "Deployment {{ $labels.deployment }} en namespace {{ $labels.namespace }} lleva 30 minutos sin tener las réplicas esperadas."
```

![Alerta 06](./01-practica/img/alerta_06.png)

### 07 Endpoints sin direcciones disponibles durante más de 10 minutos

```yaml
- alert: 07_endpoint_sin_direcciones_disponibles_10_min
  expr: kube_endpoint_address_available{namespace="practica"} == 0
  for: 10m
  labels:
    team: "sistemas"
    severity: critical
  annotations:
    summary: "Endpoint no tiene direcciones disponibles"
    description: "Deployment {{ $labels.endpoint }} en namespace {{ $labels.namespace }} lleva 10 minutos sin tener direcciones disponibles."
```

![Alerta 07](./01-practica/img/alerta_07.png)

## Las alarmas tendrán Severity "high" o "critical"

Severity:

* "high"
  * contenedor_pasa_90_pct_limite_cpu_15_min
  * contenedor_pasa_80_pct_limite_mem_15_min
  * contenedor_debajo_50_pct_solicitado_cpu_15_min
  * contenedor_debajo_50_pct_solicitado_mem_15_min
  * replicas_deployment_diferente_esperadas_30_min
* "critical"
  * pod_no_running_15_min
  * endpoint_sin_direcciones_disponibles_10_min

## Alert Manager

La configuración de Alert Manager se puede consultar en:

* [http://monitoring-alertmanager.35.205.124.111.nip.io/#/status](http://monitoring-alertmanager.35.205.124.111.nip.io/#/status)

### Envío de alertas con label "severity: critical" a "oncall-critical@example.com"

En la configuración, se filtran las alertas de "severity: critical" al receptor "sistemas_critical":

```yaml
- receiver: sistemas_critical
  match:
    severity: critical
```

Y se define el receptor "sistemas_critical" para el envío del correo a "oncall-critical@example.com":

```yaml
- name: "sistemas_critical"
  email_configs:
    - send_resolved: true
      headers:
        subject: '{{ template "email.subject" . }}'
        to: "oncall-critical@example.com"
      html: '{{ template "email.body" . }}'
      text: '{{ template "email.body" . }}'
```

### Envío de alertas con label "severity: high" a "oncall-high@example.com"

En la configuración, se filtran las alertas de "severity: high" al receptor "sistemas_high":

```yaml
- receiver: sistemas_high
  match:
    severity: high
```

Y se define el receptor "sistemas_high" para el envío del correo a "oncall-high@example.com":

```yaml
- name: "sistemas_high"
  email_configs:
    - send_resolved: true
      headers:
        subject: '{{ template "email.subject" . }}'
        to: "oncall-high@example.com"
      html: '{{ template "email.body" . }}'
      text: '{{ template "email.body" . }}'
```

### Envío de alarmas y envío de recuperación de alarmas

En los puntos anteriores se define el envío de alarmas por correo electrónico, y, con la siguiente línea, se establece la notificación también del restablecimiento de la alarma:

```yaml
- send_resolved: true
```

### Envío de correo electrónico utilizando una plantilla para cuerpo y asunto del mensaje, con la información de las alarmas

La plantila define el valor de los campos para el cuerpo y el asunto del mensaje de correo electrónico

```yaml
templateFiles:
  plantilla-email.tmpl: |-
    {{ define "email.subject" }}{{ .Annotations.summary }}{{ end }}
    {{ define "email.body" }}
    {{ range .Alerts -}}
    - {{ .Annotations.description }}
    {{ end }}
    {{ end }}
```

## Grafana

Se pueden observar las métricas con [Grafana](https://grafana.com/), con el origen de datos de [Prometheus](https://prometheus.io/), tras importar el [Dashboard (JSON)](01-practica/monitoring/Dashboard.json), en la siguiente dirección:

* [http://monitoring-grafana.35.205.124.111.nip.io/](http://monitoring-grafana.35.205.124.111.nip.io/):
  * Usuario: "admin".
  * Contraseña: "prom-operator".

Se han definido las siguientes variables "fijas" para acotar los resultados en los ejercicios.

* $datasource:
  * Oculta.
  * Valor fijo "Prometheus".

![Métricas Grafana Datasource](./01-practica/img/metricas_datasource.png)

* $namespace:
  * Oculta.
  * Valor fijo "practica".

![Métricas Grafana Namespace](./01-practica/img/metricas_namespace.png)

* $intervalo:
  * Visible.
  * Valores fijos: "1m, 5m, 10m, 30m, 1h, 6h, 12h, 1d, 7d, 14d, 30d".

![Métricas Grafana Intervalo](./01-practica/img/metricas_intervalo.png)

**Para ligar los resultados de "Deployments" y de "Pods" en las próximas variables, se ha utilizado el campo "label_run".**

![Métricas Grafana Dashboard](./01-practica/img/metricas_dashboard.png)

### Variable con deployments disponibles

Query:

```yaml
label_values(kube_deployment_labels{namespace="$namespace"}, label_run)
```

![Métricas Grafana Deployments](./01-practica/img/metricas_deployments.png)

### Variable con los pods del deployment

Query:

```yaml
label_values(kube_pod_labels{namespace="$namespace",label_run="$deployment"}, pod)
```

![Métricas Grafana Pods](./01-practica/img/metricas_pods.png)

### Panel con uso sumado de CPU de todos los Pods seleccionados en la variable

Query:

```yaml
sum(rate(container_cpu_usage_seconds_total{pod_name=~"$pod"}[$intervalo]))
```

![Métricas Grafana CPU](./01-practica/img/metricas_cpu.png)

### Panel con el uso sumado de memoria de todos los Pods seleccionados en la variable

Query:

```yaml
sum(container_memory_usage_bytes{pod_name=~"$pod"} /1024/1024)
```

![Métricas Grafana Memoria](./01-practica/img/metricas_memoria.png)

### Panel con el número de reinicios sumado de todos los pods seleccionados en la variable

Query:

```yaml
sum(kube_pod_container_status_restarts_total{pod="$pod"})
```

![Métricas Grafana Reinicios](./01-practica/img/metricas_reinicios.png)

Seguir a la [Segunda Parte](./README_PARTE_2.md) de la práctica
