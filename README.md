# Práctica Módulo 07 - Liberando Productos

Proyecto académico con el objetivo de utilizar una arquitectura en Google Cloud Platform que ponga de manifiesto las buenas prácticas en la liberación de productos, ([enunciado](./Enunciado-Practica_Liberando_Productos.pdf)).

## Instalación de requisitos

Se ha comprobado la ejecución correcta de los ejercicios en un sistema operativo [Debian 9](https://www.debian.org/index.es.html), con las siguientes aplicaciones previamente instaladas:

* [Google Cloud SDK](https://cloud.google.com/sdk/install) 264.0.0.
  * alpha 2019.09.22.
  * beta 2019.0.22.
  * bq 2.0.47.
  * core 2019.09.22.
  * gsutil 4.42.
  * kubectl 2019.09.22.
* [Docker](https://docs.docker.com/install/linux/docker-ce/debian/) 19.03.2, build 6a30dfca03.
* [Docker Compose](https://docs.docker.com/compose/install/) 1.24.1, build 4667896b.
* [Git](https://www.git-scm.com/) 2.11.0.

Instalar "*Google Cloud SDK*":

```bash
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update && sudo apt-get install google-cloud-sdk -y
```

![Instalar Google Cloud SDK](./00-requisitos/img/instalar-gcloud-01.png)

Inicializar el SKD "*Google Cloud SDK*":

```bash
gcloud init
```

![Inicializar Google Cloud SDK 1](./00-requisitos/img/inicializar-gcloud-01.png)

Hacer login con la guenta de Google:

![Inicializar Google Cloud SDK 2](./00-requisitos/img/inicializar-gcloud-02.png)

![Inicializar Google Cloud SDK 3](./00-requisitos/img/inicializar-gcloud-03.png)

Copiar el código obtenido en la consola:

![Inicializar Google Cloud SDK 4](./00-requisitos/img/inicializar-gcloud-04.png)

Seleccionar el proyecto "*practica-kc-07-libeproduc*":

![Inicializar Google Cloud SDK 5](./00-requisitos/img/inicializar-gcloud-05.png)

Escoger como region por defecto la *17* ("*europe-west1-b*"):

![Inicializar Google Cloud SDK 6](./00-requisitos/img/inicializar-gcloud-06.png)

![Inicializar Google Cloud SDK 7](./00-requisitos/img/inicializar-gcloud-07.png)

Comprobar la instalación del SKD "*Google Cloud SDK*":

```bash
gcloud --version
```

![Comprobar Google Cloud SDK](./00-requisitos/img/comprobar-gcloud-01.png)

Instalar *[kubectl](https://kubernetes.io/es/docs/tasks/tools/install-kubectl/)*:

```bash
sudo apt-get install kubectl -y
```

![Instalar kubectl](./00-requisitos/img/instalar-kubectl-01.png)

Instalar "*Git*":

```bash
sudo apt-get install git -y
```

![Instalar Git](./00-requisitos/img/instalar-git-01.png)

Dar acceso completo al proyecto "*practica-kc-07-libeproduc*" al usuario "kc.boot.devops.03@gmail.com", con rol "*editor*":

```bash
gcloud projects add-iam-policy-binding practica-kc-07-libeproduc --member user:kc.boot.devops.03@gmail.com --role roles/editor
```

![Dar acceso proyecto](./00-requisitos/img/dar-acceso-proyecto-editor-01.png)

## Crear clúster en GKE e instalar la práctica

Clonar este repositorio en el directorio "*practica*":

```bash
mkdir practica
cd practica
git clone https://gitlab.com/aureliopn/07_libeproduc.git
cd ..
```

![Clonar repositorio Git](./00-requisitos/img/clonar-repositorio-01.png)

Ejecutar el script [crear_cluster_practica.sh](./01-practica/crear_cluster_practica.sh) que crea el clúster con la práctica instalada:

```bash
cd practica/libeproduc/01-practica
sudo chmod 700 *.sh
./crear_cluster_practica.sh >> log.txt
cd ../../..
```

En caso de encontrar algún problema, se puede estudiar la salida generada en el archivo [log.txt](./01-practica/log.txt).

Seguir a la [Primera Parte](./README_PARTE_1.md) de la práctica
